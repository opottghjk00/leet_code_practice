
#include<iostream>

using namespace std;


class Solution {
public:
    Solution(int stair){n = stair;}
    int climbStairs() {
        if (n <= 2) {return n;}
        else{
            int prev2 = 1;
            int prev1 = 2;
            int result;
            for (int i = 3; i <= n; i++){
                if (i == n){
                    result =  prev2 + prev1;
                }
                else{
                    prev1 = prev1 + prev2;
                    prev2 = prev1 - prev2;
                }
            }
            return result;
        }
    }
private:
    int n;
};

int main(){
    Solution example(4);
    cout << example.climbStairs() << endl;
}
